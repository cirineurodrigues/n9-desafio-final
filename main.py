class Moradia:
    def __init__(self, area: int, endereco: str, numero: int, preco_imovel: int, tipo = 'Não especificado'):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = tipo
    
    def gerar_relatorio_preco(self):
        return int(self.preco_imovel / self.area)

class Apartamento(Moradia):
    tipo = 'Apartamento'
    def __init__(self, area: int, endereco: str, numero: int, preco_imovel: int, preco_condominio: int, num_apt: int, num_elevadores: int):
        super().__init__(area, endereco, numero, preco_imovel, tipo = Apartamento.tipo)

        self.preco_condominio = preco_condominio
        self.num_apt = num_apt
        self. num_elevadores = num_elevadores
        self.andar = self.num_apt // 10

    def gerar_relatorio(self):
        return f'{self.endereco} - apt {self.num_apt} = andar: {self.andar} - elevadores: {self.num_elevadores} - preco por m²: R$ {self.gerar_relatorio_preco()}'